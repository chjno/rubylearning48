=begin 
Write a method called convert that takes one argument which is a
temperature in degrees Fahrenheit. This method should return the temperature
in degrees Celsius.

To format the output to say 2 decimal places, we can use
the Kernel's format method. For example, if x = 45.5678 then format("%.2f", x)
will return the string 45.57. Another way is to use the round function as
follows:

puts (x*100).round/100.0
=end

# doctest: convert to celsius
# >> convert(212)
# => 100
# >> convert(-40)
# => -40
# >> convert(100) != 100
# => true

def convert(fahrenheit)
  (Float(fahrenheit) - 32) * 5 / 9
end

# doctest: convert to celsius
# >> convert_to_celsius(100) == convert(100)
# => true

def convert_to_celsius(fahrenheit)
  convert(fahrenheit)
end

if __FILE__ == $PROGRAM_NAME
  puts "Please enter a temperature in Fahrenheit to convert to Celsius:"
  fahrenheit = gets.chomp
  celsius = convert_to_celsius(fahrenheit)
  puts "#{fahrenheit} degrees Fahrenheit is equal to %.2f Celsius" % celsius
end
