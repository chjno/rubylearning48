# Write a Ruby program that displays how old I am, if I am 979000000 seconds
# old. Display the result as a floating point (decimal) number to two decimal
# places (for example, 17.23). Note: To format the output to say 2 decimal
# places, we can use the Kernel's format method. For example, if x = 45.5678
# then format("%.2f", x) will return the string 45.57

puts 'Enter an age in seconds to convert to years:'
age_in_secs = gets.chomp
age_to_convert = Float(age_in_secs)
secs_in_a_year = 365 * 24 * 60 * 60
age_in_years = age_to_convert / secs_in_a_year
puts sprintf '%.2f seconds is equal to %.2f years old', age_in_secs,
                                                          age_in_years
