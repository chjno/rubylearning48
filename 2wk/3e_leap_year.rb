# Write a method leap_year?. It should accept a year value from the user, check
# whether it's a leap year, and then return true or false. With the help of
# this leap_year?() method calculate and display the number of minutes in a
# leap year (2000 and 2004) and the number of minutes in a non-leap year (1900
# and 2005).  Note: Every year whose number is divisible by four without a
# remainder is a leap year, excepting the full centuries, which, to be leap
# years, must be divisible by 400 without a remainder. If not so divisible they
# are common years. 1900, therefore, is not a leap year.

# doctest: leap year
# >> leap_year?(2000)
# => true
# >> leap_year?(1996)
# => true
# >> leap_year?(1999)
# => false
# >> leap_year?(1900)
# => false

def leap_year?(year)
  year % 400 == 0 || year % 4 == 0 && year % 100 != 0
end

# doctest: minutes in year
# >> minutes_in_year(2000)
# => 527040
# >> minutes_in_year(1996)
# => 527040
# >> minutes_in_year(1999)
# => 525600
# >> minutes_in_year(1900)
# => 525600

def minutes_in_year(year)
  24 * 60 * (leap_year?(year) ? 366 : 365)
end

if __FILE__ == $PROGRAM_NAME
  years = [2000, 2004, 1900, 2005]
  years.each do |year|
    puts "There are #{minutes_in_year(year)} minutes in the year #{year}"
  end
end
