# Run the following two programs and try and understand the difference in the
# outputs of the two programs. The program:

# def mtdarry
#  10.times do |num|
#  puts num
#  end
# end
# mtdarry
# 
# and the program:
# 
# def mtdarry
#  10.times do |num|
#  puts num
#  end
# end
# puts mtdarry

# Solution:
# The return value is the original integer used.
# The first program outputs 0-9 as it goes through the loop.
# The second program does that and also puts the return value, 10. Return value
# is nil because the last line is puts, which has a return value of nil.
