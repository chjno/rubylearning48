# Write a Ruby program that prints the numbers from 1 to 100. But for multiples
# of three print "Fizz" instead of the number and for multiples of five print
# "Buzz". For numbers which are multiples of both three and five print
# "FizzBuzz". Discuss this in the FizzBuzz Forum.

(1..100).to_a.each do |number|
  output = ''
  output << 'Fizz' if number % 3 == 0
  output << 'Buzz' if number % 5 == 0
  output = number if output == ''
  puts output
end
