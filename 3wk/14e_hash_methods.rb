# Select all answers which return true.
# 
# h = { "Ruby" => "Matz", "Perl" => "Larry", "Python" => "Guido" }
#  
# Answers:
#  
# 1. h.member?("Matz")
# 2. h.member?("Python")
# 3. h.include?("Guido")
# 4. h.include?("Ruby")
# 5. h.has_value?("Larry")
# 6. h.exists?("Perl")

# Solution:
#
# 2,4,5 will return true.
# member? and include? return true if the given key exists.
# has_value? returns true if the given value exists.
# exists? is not a valid hash method.
