# Write a Ruby program that, when given an array: collection = [12, 23, 456,
# 123, 4579] prints each number, and tells you whether it is odd or even.

collection = [12, 23, 456, 123, 4579]
collection.each do |number|
  print number.to_s + ' is '
  puts number.even? ? 'even.' : 'odd.'
end
