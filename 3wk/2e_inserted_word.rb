# A plain text file has the following contents:
#
# text text text text text
# text text text text text
# text text word text text
# text text text text text
# text text text text text
#
# Observe that in this file, there exists a word 'word'. Write a clever but
# readable Ruby program that updates this file and the final contents become
# like this:
#
# text text text text text
# text text text text text
# text text inserted word text text
# text text text text text
# text text text text text
#
# Do not hard-code the file name.

def inserted_word(file_name)
  contents = File.read(file_name)
  contents.gsub! 'word', 'inserted word'
  File.open(file_name, 'w') do |f|
    f.puts contents
  end
end

puts 'please enter file names separated by spaces:'
files = gets.chomp.split
files.each do |file|
  inserted_word(file)
end
