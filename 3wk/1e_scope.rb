Why is the output of this program as shown below in the comment?

def method
  a = 50
  puts a
end
 
a = 10
method
puts a
 
=begin
This displays like so:
 
50
10
=end

# the value of a in the method is local to that method so it doesn't affect the previous assignment of a = 10. First you get the ouput 50 because the method is called and the value of a = 50 inside the method, and then when you puts a you get the original value that it was assigned outside of the method. 
