# Given the following Ruby code snippet:
# 
# a = (1930...1951).to_a
# puts a[rand(a.size)]
# 
# When you run this program, which of the following values will not be displayed?
# 1929
# 1930
# 1945
# 1950
# 1951
# 1952
# Explain why that value will not be displayed.
# 
# Also, have a look at the splat operator: a = [*1930...1951] # splat operator

Answer:
1929, 1951, 1952 will not be displayed because the range only consists of numbers between 1930 and 1951, not including 1951 because the three dots is a non-inclusive range. since rand produces a random integer from 0 to a specified integer argument, including 0 and not including the argument, this corresponds to the indices of the numbers in the array - 0 to argument - 1 is the same as 0 to a.size - 1. so any number in the array will be displayed.
