# 1. Load in a file containing the text or document you want to analyze.
# 2. As you load the file line by line, keep a count of how many lines there
# are (one of your statistics taken care of).
# 3. Put the text into a string and measure its length to get your character count.
# 4. Temporarily remove all whitespace and measure the length of the resulting
# string to get the character count excluding spaces.
# 5. Split on whitespace to find out how many words there are.
# 6. Split on full stops (.), '!' and '?' to find out how many sentences there are.
# 7. Split on double newlines to find out how many paragraphs there are.
# 8. Perform calculations to work out the averages.
#      Average number of words per sentence
#      Average number of sentences per paragraph

File.open('text.txt', 'r') do |f|
  line_count = 0
    line_count += 1
end
text = File.read('text.txt')
char_count = text.length.to_f
char_count_minus_spaces = text.gsub(/\s+/, '').length.to_f
word_count = text.split(/\s+/).length.to_f
sentence_count = text.split(/\.|\?|!/).length.to_f
paragraph_count = text.split(/\n\n/).length.to_f
avg_words_per_sentence = word_count / sentence_count.to_f
avg_sentences_per_paragraph = sentence_count / paragraph_count.to_f

puts "Character Count: #{char_count}"
puts "Character Count Minus Spaces: #{char_count_minus_spaces}"
puts "Word Count: #{word_count}"
puts "Sentence Count: #{sentence_count}"
puts "Paragraph Count: #{paragraph_count}"
puts "Average Words Per Sentence: #{avg_words_per_sentence}"
puts "Average Sentences Per Paragraph: #{avg_sentences_per_paragraph}"
