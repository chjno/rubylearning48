# Take two text files say A and B. The program should swap the contents of A
# and B. That is after the program is executed, A should contain B's contents
# and B should contains A's contents.

puts 'Please enter two text file names separated by spaces:'
files = gets.chomp.split
contents1 = File.read(files[0])
contents2 = File.read(files[1])
File.open(files[0], 'w') do |f|
  f.puts contents2
end
File.open(files[1], 'w') do |f|
  f.puts contents1
end
