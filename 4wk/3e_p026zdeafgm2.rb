# Modify your Deaf Grandma program (Week 3 / Exercise6): What if grandma
# doesn't want you to leave? When you shout BYE, she could pretend not to hear
# you. Change your previous program so that you have to shout BYE three times
# in a row. Make sure to test your program: if you shout BYE three times, but
# not in a row, you should still be talking to grandma. You must shout BYE
# three separate times. If you shout BYEBYEBYE or BYE BYE BYE, grandma should
# pretend not to hear you (and not count it as a BYE).

years = (1930..1950).to_a
puts 'Say something to grandma:'
you_say = gets.chomp
loop do
  if you_say == 'BYE'
    i = 0
    loop do
      i += 1
      abort if i == 3
      puts 'HUH?! SPEAK UP, SONNY!'
      you_say = gets.chomp
      break if you_say != 'BYE'
    end
  elsif you_say == ''
  elsif you_say != you_say.upcase
    puts 'HUH?! SPEAK UP, SONNY!'
  else
    puts "NO, NOT SINCE #{years[rand(years.size)]}"
  end
  you_say = gets.chomp
end
