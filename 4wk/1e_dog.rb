# Write a class called Dog, that has name as an instance variable and the
# following methods:
# 
# bark(), eat(), chase_cat()
# I shall create the Dog object as follows:
# d = Dog.new('Leo')

# doctest: setup
# >> pluto = Dog.new('pluto')
class Dog
  def initialize(name)
    @name = name
  end

  # doctest: Must be able to bark
  # >> pluto.bark
  # => 'woof!'
  def bark
    'woof!'
  end

  # doctest: Must be able to eat
  # >> pluto.eat
  # => 'nom nom'
  def eat
    'nom nom'
  end

  # doctest: Must be able to chase cats
  # >> pluto.chase_cat
  # => 'grrrr'
  def chase_cat
    'grrrr'
  end
end

if __FILE__ == $PROGRAM_NAME
  spot = Dog.new('Spot')
  puts spot.bark
  puts spot.eat
  puts spot.chase_cat
end
