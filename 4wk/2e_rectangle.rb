# Write a Rectangle class. I shall use your class as follows:
# 
# r = Rectangle.new(23.45, 34.67)
# puts "Area is = #{r.area}"
# puts "Perimeter is = #{r.perimeter}"

# doctest: rectangle class
# >> rect = Rectangle.new(5, 10)
class Rectangle
  def initialize(width, length)
    @width = width
    @length = length
  end

  # doctest: calculate area
  # >> rect.area
  # => 50
  def area
    @width * @length
  end

  # doctest: calculate perimeter
  # >> rect.perimeter
  # => 30
  def perimeter
    2 * (@width + @length)
  end
end

if __FILE__ == $PROGRAM_NAME
  r = Rectangle.new(23.45, 34.67)
  puts "Area is = #{r.area}"
  puts "Perimeter is = #{r.perimeter}"
end
